const range = document.getElementById("range");
const bubble = document.getElementById("bubble");
const rangeV = document.getElementById("rangeV");

const setValue = ()=>{
    const
        newValue = Number( (range.value - range.min) * range.max / (range.max - range.min) ),
        newPosition = 5.40 - (newValue * 0.02);

    const trayectory = Number( (range.value - range.min) * 100 / (range.max - range.min));
    const ranges = {
        pequeño: {value: 20, message: "Paquete pequeño"},
        express: {value: 40, message: "Paquete express"},
        mediano: {value: 50, message: "Paquete mediano"},
        grande:  {value: 60, message: "paquete grande"}
    }

    const message = (newValue < ranges.pequeño.value) ? "Paquete pequeño" :
                    (newValue < ranges.express.value) ? "Paquete express" :
                    (newValue < ranges.mediano.value) ? "Paquete mediano" : "Paquete grande";
    
    rangeV.innerHTML = `<span>${message} <span class='value_peso'>${newValue + " Kg"}</span></span>`;
    rangeV.style.left = `calc(${trayectory}% + (${newPosition}px))`;
};

const points = [
    {
        name: "Paquete pequeño",
        maxValue: 20
    },
    {
        name: "Paquete express",
        maxValue: 40
    },
    {
        name: "Paquete mediano",
        maxValue: 60
    },
    {
        name: "Paquete express",
        maxValue: 100
    }
]

document.addEventListener("DOMContentLoaded", setValue);
range.addEventListener('input', setValue);

//Input radios


const transportadoras = document.querySelector(".transportadoras");


transportadoras.addEventListener('click', (e) => {
    const nombreTransportadora = e.target.parentElement.parentElement.children[1];
    const transportadorasList = document.querySelectorAll(".transportadora span");

    for(let nombreTransporte of transportadorasList){
        nombreTransporte.classList.remove("active");
    }
    nombreTransportadora.classList.add('active');
});





